package repositories

import androidx.lifecycle.LiveData
import dao.ReturnVisitDao
import entities.ReturnVisit

class ReturnVisitRepository(private val returnVisitDao: ReturnVisitDao) {

    val allReturnVisits: LiveData<List<ReturnVisit>> = returnVisitDao.getAll()

    suspend fun insert(returnVisit: ReturnVisit) {
        returnVisitDao.insert(returnVisit)
    }
}
