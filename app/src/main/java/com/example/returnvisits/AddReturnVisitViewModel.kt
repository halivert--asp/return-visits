package com.example.returnvisits

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import entities.ReturnVisit
import kotlinx.coroutines.launch
import repositories.ReturnVisitRepository

class AddReturnVisitViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: ReturnVisitRepository
    val allReturnVisits: LiveData<List<ReturnVisit>>

    init {
        val returnVisitDao = AppDatabase.getDatabase(application, viewModelScope).returnVisitDao()
        repository = ReturnVisitRepository(returnVisitDao)
        allReturnVisits = repository.allReturnVisits
    }

    fun insert(returnVisit: ReturnVisit) = viewModelScope.launch {
        repository.insert(returnVisit)
    }
}
