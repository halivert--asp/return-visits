package com.example.returnvisits

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import entities.ReturnVisit

class ReturnVisitListAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<ReturnVisitListAdapter.ReturnVisitViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var returnVisits = emptyList<ReturnVisit>()

    inner class ReturnVisitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val returnVisitItemView: TextView = itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReturnVisitViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return ReturnVisitViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ReturnVisitViewHolder, position: Int) {
        val current = returnVisits[position]
        holder.returnVisitItemView.text = current.personName
    }

    internal fun setReturnVisits(returnVisits: List<ReturnVisit>) {
        this.returnVisits = returnVisits
        notifyDataSetChanged()
    }

    override fun getItemCount() = returnVisits.size
}

