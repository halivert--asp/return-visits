package com.example.returnvisits

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import converters.Converters
import dao.ReturnVisitDao
import dao.UserDao
import entities.ReturnVisit
import entities.User
import kotlinx.coroutines.CoroutineScope

@Database(entities = [User::class, ReturnVisit::class], version = 2, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun returnVisitDao(): ReturnVisitDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "halivert_returnvisit"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}