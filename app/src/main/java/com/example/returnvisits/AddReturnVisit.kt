package com.example.returnvisits

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import components.DateTimePicker
import entities.ReturnVisit
import java.sql.Date


class AddReturnVisit : Fragment() {

    companion object {
        fun newInstance() = AddReturnVisit()
    }

    private lateinit var viewModel: AddReturnVisitViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.add_return_visit_fragment, container, false)

        val button = view.findViewById<Button>(R.id.save_button)
        button.setOnClickListener {
            saveReturnVisit(view)
        }

        val editText = view.findViewById<EditText>(R.id.date)
        editText.setOnClickListener {
            val datePicker =
                DateTimePicker(DatePickerDialog.OnDateSetListener { _, rYear, rMonth, dayOfMonth ->
                    val dayString = dayOfMonth.toString().padStart(2, '0')
                    val monthString = (rMonth + 1).toString().padStart(2, '0')
                    view.findViewById<EditText>(R.id.date)
                        .setText("${dayString}-${monthString}-$rYear")
                }, null)
            activity?.supportFragmentManager?.let { it ->
                datePicker.show(it, "datePicker")
            }
        }

        return view
    }

    fun saveReturnVisit(view: View) {
        val date = Date.valueOf(view.findViewById<EditText>(R.id.date).text.toString())

        Log.e("Error", date.toString())

        return
        val personName = view.findViewById<EditText>(R.id.person_name).text.toString()
        val territoryNumber =
            view.findViewById<EditText>(R.id.territory_number).text.toString().toInt()
        val territoryDescription =
            view.findViewById<EditText>(R.id.territory_description).text.toString()
        val house = view.findViewById<EditText>(R.id.house).text.toString()
        val exterior = view.findViewById<EditText>(R.id.exterior).text.toString()
        val description = view.findViewById<EditText>(R.id.description).text.toString()

        val returnVisit = ReturnVisit(
            personName,
            territoryNumber,
            territoryDescription,
            house,
            exterior,
            date,
            description
        )

        viewModel.insert(returnVisit)
    }
}
