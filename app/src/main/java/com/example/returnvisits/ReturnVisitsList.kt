package com.example.returnvisits

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ReturnVisitsList : Fragment() {
    private var listenerReturnVisitsList: OnReturnVisitsListFragmentListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.list_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.settings -> {
                listenerReturnVisitsList?.onFragmentInteraction(
                    Uri.parse("halivert://returnvisits.com/settings")
                )
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_return_visits_list, container, false)
        val fab = view.findViewById<FloatingActionButton>(R.id.floatingActionButton)
        fab.setOnClickListener {
            listenerReturnVisitsList?.onFragmentInteraction(
                Uri.parse("halivert://returnvisits.com/visit/create")
            )
        }

        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        val adapter = context?.let {
            ReturnVisitListAdapter(it)
        }

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnReturnVisitsListFragmentListener) {
            listenerReturnVisitsList = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnReturnVisitsListFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listenerReturnVisitsList = null
    }

    interface OnReturnVisitsListFragmentListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance() = ReturnVisitsList()
    }
}
