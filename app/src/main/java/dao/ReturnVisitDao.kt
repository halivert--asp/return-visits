package dao

import androidx.lifecycle.LiveData
import androidx.room.*
import entities.ReturnVisit

@Dao
interface ReturnVisitDao {
    @Query("SELECT * FROM returnvisit")
    fun getAll(): LiveData<List<ReturnVisit>>

    @Insert
    fun insert(returnVisit: ReturnVisit)

    @Update
    fun update(returnVisit: ReturnVisit)

    @Delete
    fun delete(returnVisit: ReturnVisit)
}